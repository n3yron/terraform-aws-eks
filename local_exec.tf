resource "null_resource" "example01" {
  provisioner "local-exec" {
    command = "aws eks --region $REGION update-kubeconfig --name $EKS_NAME --kubeconfig my_eks_config"

    environment = {
      REGION   = var.region
      EKS_NAME = local.cluster_name
    }
  }
  depends_on = [module.eks]
}

resource "null_resource" "example02" {
  provisioner "local-exec" {
    command = "kubectl --kubeconfig my_eks_config get nodes"

    environment = {
      REGION   = var.region
      EKS_NAME = local.cluster_name
    }
  }
  depends_on = [module.eks]
}


resource "null_resource" "example3" {
  provisioner "local-exec" {
    command = "whoami"
  }
  depends_on = [module.eks]
}

resource "null_resource" "example4" {
  provisioner "local-exec" {
    command = "pwd"
  }
  depends_on = [module.eks]
}
