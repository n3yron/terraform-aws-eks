resource "aws_route53_record" "www" {
  zone_id = "Z36VNL1RN3KWDL"
  name    = var.dns_name
  type    = "CNAME"
  ttl     = 60
  records = [data.kubernetes_service_v1.lb_dns_record.status.0.load_balancer.0.ingress.0.hostname]
  #  records = [var.myrecord]
  depends_on = [helm_release.ingress-nginx]
}

resource "aws_route53_record" "www2" {
  zone_id = "Z36VNL1RN3KWDL"
  name    = var.dns_name_2
  type    = "CNAME"
  ttl     = 60
  records = [data.kubernetes_service_v1.lb_dns_record.status.0.load_balancer.0.ingress.0.hostname]

  depends_on = [helm_release.ingress-nginx]
}
# MYIP="$(kubectl get svc ingress-nginx-controller | grep -v NAME | awk '{print $4}')"
# terraform apply --auto-approve -var="myrecord=3.3.3.3"
# terraform apply --auto-approve -var="myrecord=$MYIP" -var="dns_name=n3yron.nebula.video"
variable "myrecord" {
  default = "5.5.5.5"
}

variable "dns_name" {
  default = "n3yron.nebula.video"
}
variable "dns_name_2" {
  default = "mediacms.nebula.video"
}

# AWS
# MYIP="$(kubectl --kubeconfig=my_eks_config get svc nginx-ingress-controller-ingress-nginx-controller | grep -v NAME | awk '{print $4}')"
data "kubernetes_service_v1" "lb_dns_record" {
  metadata {
    name = "nginx-ingress-controller-ingress-nginx-controller"
  }
  depends_on = [aws_eks_cluster.eks_cluster, helm_release.ingress-nginx]
}

output "lb" {
  value = data.kubernetes_service_v1.lb_dns_record.status.0.load_balancer.0.ingress.0.hostname
}
# Main RDS
data "aws_db_instance" "database" {
  db_instance_identifier = aws_db_instance.rds.identifier
}
output "rds" {
  value = data.aws_db_instance.database.address
}

resource "aws_route53_record" "rds_record" {
  zone_id = "Z36VNL1RN3KWDL"
  name    = "${var.my_cluster_name}-rds-main.nebula.video"
  type    = "CNAME"
  ttl     = 60
  records = [data.aws_db_instance.database.address]
  #  records = [var.myrecord]

  depends_on = [helm_release.ingress-nginx]
}

variable "rds_dns_name" {
  default = "n3yron-east-rds.nebula.video"
}
# Replica RDS
data "aws_db_instance" "database2" {
  db_instance_identifier = aws_db_instance.rds2.identifier
}
resource "aws_route53_record" "rds_record2" {
  zone_id = "Z36VNL1RN3KWDL"
  name    = "${var.my_cluster_name}-rds-replica.nebula.video"
  type    = "CNAME"
  ttl     = 60
  records = [data.aws_db_instance.database2.address]
  #  records = [var.myrecord]

  depends_on = [helm_release.ingress-nginx]
}
# Global RDS DNS name
resource "aws_route53_record" "rds_record_global" {
  zone_id = "Z36VNL1RN3KWDL"
  name    = "${var.my_cluster_name}-rds-global.nebula.video"
  type    = "CNAME"
  ttl     = 60
  records = [aws_route53_record.rds_record.name]
  #  records = [var.myrecord]

  depends_on = [helm_release.ingress-nginx, aws_route53_record.rds_record, aws_route53_record.rds_record2]
}
