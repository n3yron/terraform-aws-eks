resource "aws_db_instance" "rds2" {
  replicate_source_db = aws_db_instance.rds.identifier
  #  replicate_source_db     = aws_db_instance.rds.arn
  instance_class          = "db.m6i.large"
  identifier              = "mediacms-replica"
  parameter_group_name    = "default.postgres13"
  skip_final_snapshot     = true
  publicly_accessible     = true
  backup_retention_period = 0
  #  db_subnet_group_name    = aws_db_subnet_group.rds_subnet_group2.name # No need, if replica in the same region as master
  vpc_security_group_ids = [aws_security_group.rds.id]
  tags = {
    VPC_Name = var.vpc_name
    Owner    = var.owner
    Dept     = var.dept_id
  }
}

resource "aws_db_subnet_group" "rds_subnet_group2" {
  name       = "main2"
  subnet_ids = [aws_subnet.public_subnets["public_subnet_1"].id, aws_subnet.public_subnets_2["public_subnet_2"].id, aws_subnet.private_subnets["private_subnet_1"].id, aws_subnet.private_subnets["private_subnet_2"].id]

  tags = {
    VPC_Name = var.vpc_name
    Owner    = var.owner
    Dept     = var.dept_id
  }
}



#resource "aws_security_group" "rds" {
#  name        = "allow_rds_traffic"
#  description = "Allow rds inbound traffic"
#  vpc_id      = aws_vpc.main-vpc.id

#  ingress {
#    description = "RDS"
#    from_port   = 5432
#    to_port     = 5432
#    protocol    = "tcp"
#    cidr_blocks = ["0.0.0.0/0"]
#  }

#  egress {
#    from_port        = 0
#    to_port          = 0
#    protocol         = "-1"
#    cidr_blocks      = ["0.0.0.0/0"]
#    ipv6_cidr_blocks = ["::/0"]
#  }
#
#  tags = {
#    VPC_Name = var.vpc_name
#    Owner    = var.owner
#    Dept     = var.dept_id
#  }
#}
