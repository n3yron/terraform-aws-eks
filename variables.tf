variable "region" {
  default     = "us-east-2"
  description = "AWS region"
}

variable "my_cluster_name" {
  default = "n3yron-eks"
}

variable "my_vpc" {
  default = "n3yron-vpc"
}
