provider "helm" {
  kubernetes {
    config_path = "my_eks_config"
  }
}

resource "helm_release" "nginx_ingress" {
  name = "nginx-ingress-controller"

  repository = "https://charts.bitnami.com/bitnami"
  chart      = "nginx-ingress-controller"

  #  set {
  #    name  = "service.type"
  #    value = "ClusterIP"
  #  }
  depends_on = [module.eks, resource.null_resource.example01]
}
